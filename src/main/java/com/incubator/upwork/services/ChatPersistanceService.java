package com.incubator.upwork.services;

import java.sql.Date;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.incubator.upwork.data.model.Chat;
import com.incubator.upwork.data.model.ChatMessage;
import com.incubator.upwork.data.model.ChatRequestObject;
import com.incubator.upwork.data.repository.ChatMessageRepository;
import com.incubator.upwork.data.repository.ChatRepository;

@Service
public class ChatPersistanceService {

    @Autowired
    private ChatRepository chatRepository;

    @Autowired
    private ChatMessageRepository chatMessageRepository;

    @Autowired
    private FreelancerService freelancerService;

    @Autowired
    private ClientService clientService;

    public String getCurrentTime() {
        LocalTime localTime = LocalTime.now();
        DateTimeFormatter formatterLocalTime = DateTimeFormatter.ofPattern("HH:mm:ss");
        String currenttime = formatterLocalTime.format(localTime);

        return currenttime;
    }

    public String getCurrentDate() {
        LocalDate localDate = LocalDate.now();
        DateTimeFormatter formatterLocalDate = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String currentdate = formatterLocalDate.format(localDate);
        return currentdate;
    }

    public <T> ResponseEntity<?> getConnections(Map<String, String> entity) {
        String userType = entity.get("userType");
        String email = entity.get("email");
        // Sort sort = Sort.by(Sort.Order.desc("date"), Sort.Order.desc("time"));
        if (userType.equals("freelancer")) {
            List<Chat> chats = chatRepository.findAllByFreelancerEmail(email);
            return ResponseEntity.ok().body(chats);
        }
        List<Chat> chats = chatRepository.findAllByClientEmail(email);

        return ResponseEntity.ok().body(chats);

    }

    public ChatMessage saveChatMessage(ChatRequestObject chatRequestObject) {

        Chat chat = chatRepository.getChatByClientEmailAndFreelancerEmail(chatRequestObject.getReceiverEmail(),
                chatRequestObject.getSenderEmail()).get();

        ChatMessage chatMessage = ChatMessage.builder().chat(chat).date(Date.valueOf(getCurrentDate())).seen(1)
                .senderEmail(chatRequestObject.getSenderEmail()).messageData(chatRequestObject.getMessageData())
                .time(Time.valueOf(getCurrentTime())).receiverEmail(chatRequestObject.getReceiverEmail()).build();

        // set last message in chat object and increase unseen message count
        if (freelancerService.existsByEmail(chatRequestObject.getSenderEmail())) {
            chat.setClientUnseenMessageCount((chat.getClientUnseenMessageCount() + 1));
        } else {
            chat.setFreelancerUnseenMessageCount(chat.getFreelancerUnseenMessageCount() + 1);
        }
        chat.setLastMessage(chatRequestObject.getMessageData());
        chatRepository.save(chat);

        return chatMessageRepository.save(chatMessage);
    }

    public <T> ResponseEntity<?> getMessages(Map<String, String> entity) {
        String sender = entity.get("sender");
        String receiver = entity.get("receiver");

        // send messages history
        List<ChatMessage> chatMessages = chatMessageRepository.getAllBySenderAndReceiver(sender, receiver);
        
        // make unseenCount 0;
        Chat chat = chatRepository.getChatByClientEmailAndFreelancerEmail(sender, receiver).get();
        if (freelancerService.existsByEmail(receiver)) {
            chat.setFreelancerUnseenMessageCount(0);
        } else {
            chat.setClientUnseenMessageCount(0);
        }
        chatRepository.save(chat);

        // set seen 0 
        // chatMessageRepository.setReceiverSeenZero(sender, receiver);

        return ResponseEntity.ok().body(chatMessages);
    }

}
