package com.incubator.upwork.services;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.incubator.upwork.data.model.Category;
import com.incubator.upwork.data.model.Client;
import com.incubator.upwork.data.model.Notification;
import com.incubator.upwork.data.model.SkillTags;
import com.incubator.upwork.data.model.Transction;
import com.incubator.upwork.data.repository.CategoryRepository;
import com.incubator.upwork.data.repository.ClientRepository;
import com.incubator.upwork.data.repository.FreelancerRepository;
import com.incubator.upwork.data.repository.NotificationRepository;
import com.incubator.upwork.data.repository.SkillTagsRepository;
import com.incubator.upwork.data.repository.TransctionRepository;

@Service
public class GenericService {

    @Autowired
    private SkillTagsRepository skillTagsRepository;
    @Autowired
    private NotificationRepository notificationRepository;
    @Autowired
    private FreelancerRepository freelancerRepository;
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private TransctionRepository transactionRepository;

    static Map<String, String> response = new WeakHashMap<>();

    public ResponseEntity<?> NotificationClient(Map<String, String> entity) {
        Integer id = 0;
        Map<String, List<Notification>> ret = new WeakHashMap<>();
        System.out.println(entity);
        id = Integer.parseInt(entity.get("key"));
        System.out.println("Befo4re viewed");
        List<Notification> notificationsViewed = notificationRepository
                .findAllByClientView(id, 1);
           System.out.println("Befo4re Notviewed");
        List<Notification> notificationsNotViewed = notificationRepository
                .findAllByClientView(id, 0);

        ret.put("viewed", notificationsViewed);
        ret.put("notViewed", notificationsNotViewed);

        for (Notification part : notificationsNotViewed) {
            part.setViewed(1);
            notificationRepository.save(part);
        }
        return ResponseEntity.ok().body(ret);
    }

    public ResponseEntity<?> NotificationFreelancer(Map<String, String> entity) {
        Integer id = 0;
        Map<String, List<Notification>> ret = new WeakHashMap<>();

        id = Integer.parseInt(entity.get("key"));
        List<Notification> notificationsViewed = notificationRepository
                .findAllByFreelancerView(id, 1);
        List<Notification> notificationsNotViewed = notificationRepository
                .findAllByFreelancerView(id, 0);
        ret.put("viewed", notificationsViewed);
        ret.put("notViewed", notificationsNotViewed);

        for (Notification part : notificationsNotViewed) {
            part.setViewed(1);
            notificationRepository.save(part);
        }
        return ResponseEntity.ok().body(ret);
    }

    public <T> ResponseEntity<?> viewedNotification(Map<String, T> entity) {
        response.clear();
        System.out.println("VIWED");
        List<Map<String, T>> notifications = (List<Map<String, T>>) entity.get("notViewed");
        Iterator itr = notifications.iterator();
        while (itr.hasNext()) {
            Map<String, ? extends Comparable> m = (Map<String, ? extends Comparable>) itr.next();
            int id = Integer.parseInt((String) m.get("notificationid"));
            Notification notification = notificationRepository.findById(id).get();
            notification.setViewed(1);
            notificationRepository.save(notification);
        }
        // int id = Integer.parseInt(entity.get("notificationid"));
        // Notification notification = notificationRepository.findById(id).get();
        // notification.setViewed(1);
        // notificationRepository.save(notification);
        response.put("message", "successful");
        return ResponseEntity.ok().body(response);
    }

    public static <T> void revlist(List<T> list) {
        // base condition when the list size is 0
        if (list.size() <= 1 || list == null)
            return;

        T value = list.remove(0);

        // call the recursive function to reverse
        // the list after removing the first element
        revlist(list);

        // now after the rest of the list has been
        // reversed by the upper recursive call,
        // add the first value at the end
        list.add(value);
    }

    public <T> ResponseEntity<?> transaction(Map<String, T> entity) {

        String email = (String) entity.get("email");
        System.out.println(email);
        List<Transction> transctions = transactionRepository.findAllByEmailOrderByTimeAndDate(email);
        System.out.println("Transction size = " + transctions.size());
        List<Map<String, T>> ret = new ArrayList<>();
        for (Transction part : transctions) {
            Map<String, T> m = new WeakHashMap<>();
            m.put("time", (T) part.getTime());
            m.put("date", (T) part.getDate());
            m.put("from", (T) part.getFrom());
            m.put("to", (T) part.getTo());
            m.put("amount", (T) Float.valueOf(part.getAmount()));
            m.put("transctionId", (T) part.getTransctionNumber());
            ret.add(m);
        }
        float balance = 0;
        String name = null;
        Map<String, T> part = new WeakHashMap<>();
        if (clientRepository.existsByEmail(email)) {
            balance = clientRepository.findByEmail(email).getBalance();
            name = new String(clientRepository.findByEmail(email).getFirstName() + " "
                    + clientRepository.findByEmail(email).getLastName());
        } else {
            balance = freelancerRepository.findByEmail(email).get().getBalance();
            name = new String(freelancerRepository.findByEmail(email).get().getFirstName() + " "
                    + freelancerRepository.findByEmail(email).get().getLastName());
        }
        // revlist(ret);
        part.put("balance", (T) Float.valueOf(balance));
        part.put("name", (T) name);
        ret.add(part);
        return ResponseEntity.ok().body(ret);
    }

    public ResponseEntity<?> NotificationClientCount(Map<String, String> entity) {

        return ResponseEntity.ok().body(notificationRepository.getClientViewCount(Integer.parseInt(entity.get("key")), 0));
    }

    public ResponseEntity<?> NotificationFreelancerCount(Map<String, String> entity) {
     
        return ResponseEntity.ok().body(notificationRepository.getFreelancerViewCount(Integer.parseInt(entity.get("key")),0));
    }

}
