package com.incubator.upwork.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.incubator.upwork.services.ChatPersistanceService;

@RestController
@RequestMapping("/upwork")
public class ChatPersistanceController {

    @Autowired
    private ChatPersistanceService chatService;

    @PostMapping("/connections")
    public <T> ResponseEntity<?> getChat(@RequestBody Map<String, String> entity) {
        System.out.println("/connections");
        return chatService.getConnections(entity);
    }

    @PostMapping("/get-messages")
    public <T> ResponseEntity<?> getMessages(@RequestBody Map<String, String> entity) {
        System.out.println("getMessages");
        return chatService.getMessages(entity);
    }

}
