package com.incubator.upwork.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;

import com.incubator.upwork.data.model.ChatMessage;
import com.incubator.upwork.data.model.ChatRequestObject;
import com.incubator.upwork.services.ChatPersistanceService;

import jakarta.persistence.Entity;

@Controller
public class ChatController {
    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @Autowired 
    private ChatPersistanceService chatPersistanceService;

    @MessageMapping("/private-message") 
    public ChatMessage recMessage(@Payload ChatRequestObject chatRequestObject) {
        System.out.println("private-message");
        System.out.println(chatRequestObject.getSenderEmail());
        System.out.println(chatRequestObject.getReceiverEmail());
        
        ChatMessage chatMessage =  chatPersistanceService.saveChatMessage(chatRequestObject);
        simpMessagingTemplate.convertAndSendToUser(chatRequestObject.getReceiverEmail(), "/private", chatMessage);

        return chatMessage;
    }
}
