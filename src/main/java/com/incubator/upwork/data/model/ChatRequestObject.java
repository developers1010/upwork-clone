package com.incubator.upwork.data.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ChatRequestObject {
	private String senderEmail;
	private String receiverEmail;
	private String messageData;
}
