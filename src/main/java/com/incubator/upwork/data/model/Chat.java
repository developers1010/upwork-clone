package com.incubator.upwork.data.model;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "chat")
public class Chat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;
    private String clientEmail;
    private String freelancerEmail;
    private String lastMessage;
    private Integer clientUnseenMessageCount;
    private Integer freelancerUnseenMessageCount;
    private String clientName;
    private String freelancerName;
    private Date date;
    private Time time;

    @OneToMany(mappedBy = "chat", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<ChatMessage> chatMessage = new ArrayList<>();

    public void addMessage(ChatMessage message) {
        chatMessage.add(message);
        message.setChat(this);
    }

    public void removeMessage(ChatMessage message) {
        chatMessage.remove(message);
        message.setChat(null);
    }

}
