package com.incubator.upwork.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.incubator.upwork.data.model.ChatMessage;

import jakarta.transaction.Transactional;

public interface ChatMessageRepository extends JpaRepository<ChatMessage,Integer> {

    // Make massages seen 0

    // @Modifying(clearAutomatically=true)
// @Transactional
// @Query("UPDATE MyEntity SET deletedAt = CURRENT_TIMESTAMP WHERE id = ?1")
// void markAsSoftDeleted(long id);

    // @Modifying()
    // @Transactional
    // @Query("UPDATE MyEntity SET deletedAt = CURRENT_TIMESTAMP WHERE id = ?1")
    // void markAsSoftDeleted(long id);
// id, date, from_email, message_data, seen, time, to_email, chat_message, client_seen, freelancer_seen

    @Transactional
    @Modifying
    @Query(value = "UPDATE chat_message cm SET cm.seen = 0 WHERE  cm.sender_email = :sender AND cm.receiver_email =:receiver ",nativeQuery=true)
    void setReceiverSeenZero(String sender , String receiver);

    @Query(value = "SELECT * FROM chat_message cm WHERE cm.sender_email =:sender AND cm.receiver_email =:receiver OR cm.sender_email =:receiver AND cm.receiver_email =:sender ORDER BY date DESC,time DESC ", nativeQuery=true)
    List<ChatMessage> getAllBySenderAndReceiver(String sender , String receiver);
//set seen=0 where sender =:sender ans receiver  =: receiver
    
}
