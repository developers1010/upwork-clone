package com.incubator.upwork.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.incubator.upwork.data.model.Client;
import com.incubator.upwork.data.model.Freelancer;
import com.incubator.upwork.data.model.Notification;
import com.incubator.upwork.data.model.Transction;

public interface NotificationRepository extends JpaRepository<Notification, Integer> {

    @Query(value = "SELECT * FROM notification n WHERE n.freelancer= :freelancer AND n.viewed= :view ORDER BY date DESC,time DESC", nativeQuery = true)
    public List<Notification> findAllByFreelancerView(Integer freelancer, int view);

    @Query(value = "SELECT * FROM notification n WHERE n.client= :client AND n.viewed= :view ORDER BY date DESC,time DESC", nativeQuery = true)
    public List<Notification> findAllByClientView(Integer client, int view);

    @Query(value = "SELECT COUNT(*) FROM notification n WHERE n.freelancer= :freelancer AND n.viewed= :view", nativeQuery = true)
    public Integer getFreelancerViewCount(Integer freelancer, int view);

    @Query(value = "SELECT COUNT(*) FROM notification n WHERE n.client= :client AND n.viewed= :view", nativeQuery = true)
    public Integer getClientViewCount(Integer client, int view);

}
