package com.incubator.upwork.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.incubator.upwork.data.model.Transction;

public interface TransctionRepository extends JpaRepository<Transction, Integer> {

    @Query(value = "SELECT * FROM transction t WHERE t.from_email = :email OR t.to_email = :email ORDER BY date DESC,time DESC", nativeQuery = true)
    public List<Transction> findAllByEmailOrderByTimeAndDate(String email);

    public boolean existsByTransctionNumber(int number);
}
