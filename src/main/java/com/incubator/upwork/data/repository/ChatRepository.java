package com.incubator.upwork.data.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.incubator.upwork.data.model.Chat;

public interface ChatRepository extends JpaRepository<Chat, Integer> {



    @Query(value = "SELECT  client_email, date, freelancer_email, last_message, time, client_name, freelancer_name, client_unseen_message_count, freelancer_unseen_message_count FROM chat ch WHERE ch.freelancer_email = :freelancerEmail ORDER BY date DESC,time DESC", nativeQuery = true)
    public List<Chat> findAllByFreelancerEmail(String freelancerEmail);
    
    @Query(value = "SELECT  client_email, date, freelancer_email, last_message, time, client_name, freelancer_name, client_unseen_message_count, freelancer_unseen_message_count FROM chat ch WHERE ch.client_email = :clientEmail ORDER BY date DESC,time DESC", nativeQuery = true)
    public List<Chat> findAllByClientEmail(String clientEmail);

    @Query(value = "SELECT * FROM chat ch WHERE ch.client_email = :clientEmail AND ch.freelancer_email = :freelancerEmail OR ch.client_email = :freelancerEmail AND ch.freelancer_email = :clientEmail", nativeQuery = true)
    public Optional<Chat> getChatByClientEmailAndFreelancerEmail(String clientEmail, String freelancerEmail);

    public Optional<Chat> findByClientEmailAndFreelancerEmail(String clientEmail, String freelancerEmail);

    

    // client_email, date, freelancer_email, last_message, time, client_name, freelancer_name, unseen_message_count
    // id, client_email, date, freelancer_email, last_message, time, client_name, freelancer_name, client_unseen_message_count, freelancer_unseen_message_count

}
